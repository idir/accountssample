import at.ran.cc.accounts.BaseAccount;
import at.ran.cc.accounts.CheckingsAccount;
import at.ran.cc.accounts.SavingsAccount;

public class App {
    public static void main(String[] args) throws Exception {
       CheckingsAccount ca = new CheckingsAccount(3000);
       ca.deposit(5000);
       ca.withdraw(2000);
       ca.withdraw(8000);
       System.out.println(ca.getBalance());

       SavingsAccount sa = new SavingsAccount();
       sa.deposit(50);
       sa.withdraw(40);
       System.out.println(sa.getBalance());
      
        
    }
}
